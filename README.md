# Some LaTeX classes for ISAE-SUPAERO lectures

## Content

This project contains some LaTeX classes to prepare documents for
SUPAERO lectures:

- `isae-exam.cls` contains a class to prepare exams. It is based on
  the [exam](https://www.ctan.org/pkg/exam "Link to CTAN page on exam class").
- `isae-exercise.cls` contains a class to prepare exercises. It is based on
  the [exam](https://www.ctan.org/pkg/exam "Link to CTAN page on exam
  class").
- `isae-refcard.cls` contains a class to define simple refcards.
- `isae-slides.cls` contains a class based
  [Beamer](https://www.ctan.org/pkg/beamer "Link to CTAN Beamer page")
  on to define slides.

**Beware:** the style, particularly for slides, does not comply to
ISAE-SUPAERO guidances for presenting documents.

## Installing

To install the files, execute

```bash
make install
```

This should install the files in your local TeXMF directory
(`~/.local/share/texmf` by default on Linux).

The `hooks` repository contains a simple hook to be linked to
`.git/hooks/post-commit` for instance to install the files when you
commit changes.

You can compile examples located in
`~/.local/share/texmf/doc/latex/supaero-lectures` with

```bash
make examples
```
